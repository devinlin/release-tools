#!/bin/bash

function get_repo_path()
{
    echo $(curl -s https://projects.kde.org/api/v1/identifier/${1} | jq -r '.repo' 2>/dev/null)
}

function get_git_rev()
{
    echo `git ls-remote kde:$repo $branch | cut -f 1`
}

function get_svn_rev()
{
    echo `svn info $1 | grep "Last Changed Rev: " | cut -f 4 -d ' '`
}

function checkDownloadUptodate()
{
    local finalDestination=$1
    local result=0
    if [ "x$force" != "x-f" ]; then
        if [ -f $finalDestination ]; then
            if [ -f $versionFilePath ]; then
                fileRepoLine=`sed -n '1p' < $versionFilePath`
                if [ "$repoLine" = "$fileRepoLine" ]; then
                    rev=`get_git_rev`
                    fileRev=`sed -n '2p' < $versionFilePath`
                    if [ "$rev" = "$fileRev" ]; then
                        fileSha=`sed -n '3p' < $versionFilePath`
                        realFileSha=`sha256sum $finalDestination`
                        if [ "$fileSha" = "$realFileSha" ]; then
                            result=1
                        fi
                    fi
                fi
            fi
        fi
    fi
    return $result
}

function grabTranslations()
{
    local basename=$1
    mkdir $basename/po
    local repo=$2
    local subdir
    for subdir in ../l10n/*; do
        local podir=$subdir/messages/$l10n_module
        if test -d $podir; then
            local lang=`basename $subdir`
            cp $podir/${repo}5.po $basename/po/$lang.po 2>/dev/null
            cp $podir/${repo}5_*.po $basename/po/$lang.po 2>/dev/null
        fi
    done
}

function setupGUITranslations()
{
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local language_list="$4"

    local found_qm=0
    local found_po=0
    MATCH_QM='^.*_qt.po$'

    while read lang; do
        po_location="${basel10n}/${lang}/messages/${module}"
        if [ -z "${po_location}" ] || [ ! -d ${po_location} ]; then
            continue
        fi

        for po_file in $(find "${po_location}" -name "*.po"); do
            # skip _desktop_, _json_, appdata, metainfo, *_xml_mimetypes
            if [[ "${po_file}" =~ (_desktop_|_json_|appdata|metainfo|_xml_mimetypes).po$ ]]; then
                continue
            elif [[ "${po_file}" =~ ${MATCH_QM} ]]; then
                # files like *_qt.po are QM in disguise
                destdir=${basedir}/poqm/${lang}
                found_qm=1
            else
                destdir=${basedir}/po/${lang}
                found_po=1
            fi
            mkdir -p "${destdir}"
            cp -f "${po_file}" "${destdir}"
        done
    done < <(cat "${language_list}")
}

function setupDOCTranslations()
{
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local language_list="$4"

    local doc_dirs=""
    local man_files=""
    #echo "DIR SCRIPT: $(readlink -f $0) - $(dirname $(readlink -f $0))"

    local translated_doc_found=0
    while read lang; do
        # set -x
        local lang_doc_dir="${basel10n}/${lang}/docs/${module}"
        if [ ! -d "${lang_doc_dir}" ]; then
            continue
        fi

        local tot_lang_docs=$(find "${lang_doc_dir}" -type f | wc -l)
        if [ ${tot_lang_docs} -eq 0 ]; then
            # no files to copy
            continue
        fi

        translated_doc_found=1

        local dest_dir="${basedir}/po/${lang}/docs/"
        mkdir -p "${dest_dir}"
        cp -r "${lang_doc_dir}/"* "${dest_dir}"

        # exceptions (difficult to generalize)
        if [ "${module}" = "khelpcenter" ]; then
            if [ -d "${dest_dir}/glossary" ]; then
                mkdir -p "${dest_dir}/khelpcenter"
                mv "${dest_dir}/glossary" "${dest_dir}/khelpcenter"
            fi
        fi
        # set +x
    done < <(cat "${language_list}")
}

function copyDataArtifacts()
{
    # Copy the element of a special directory with translated
    # artifacts (data or scripts)
    # It is an helper function for setupDataTranslations()
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local lang="$4"
    local artifacts_dir="$5"
    local out_dest_dir_var="$6"

    # ${module} is the unique repository identifier
    local lang_artifacts_dir="${basel10n}/${lang}/${artifacts_dir}/${module}"
    [ -d "${lang_artifacts_dir}" ] || return 1

    local dest_dir="${basedir}/po/${lang}/${artifacts_dir}"
    if [ ! -d "${dest_dir}" ]; then
        mkdir -p "${dest_dir}"
    fi
    cp -r "${lang_artifacts_dir}"/* "${dest_dir}"

    local relative_dest_dir="${lang}/${artifacts_dir}/${module}"
    eval ${out_dest_dir_var}="${relative_dest_dir}"
    return 0
}

function setupDataTranslations()
{
    # Takes care of both data/ and scripts/
    local basedir="$1"
    local basel10n="$2"
    local module="$3"
    local language_list="$4"

    local rel_dest_dir=""

    while read lang; do
        # process the data/ directories (and cmake_modules, if available)
        if copyDataArtifacts "${basedir}" "${basel10n}" "${module}" "${lang}" \
                             "data" "rel_dest_dir"; then
            echo "add_subdirectory(${rel_dest_dir})" >>"${basedir}/po/CMakeLists.txt"
            # copy the content of cmake_modules is available
            local cmake_modules_dir="${basel10n}/${lang}/cmake_modules"
            if [ -d "${cmake_modules_dir}" ]; then
                local cmake_dest_dir="${basedir}/po/cmake_modules"
                mkdir -p "${cmake_dest_dir}"
                cp -r "${cmake_modules_dir}/"* "${cmake_dest_dir}"
            fi
        fi
        # process the scripts/ directories (and cmake_modules, if available)
        # WARNING: this requires KI18n <=5.33 or >=5.36, or with the
        # fix which restores ki18n_install_ts_files
        if copyDataArtifacts "${basedir}" "${basel10n}" "${module}" "${lang}" \
                             "scripts" "rel_dest_dir"; then
            echo "ki18n_install_ts_files(${lang} ${rel_dest_dir})" >>"${basedir}/po/CMakeLists.txt"
        fi
    done < <(cat "${language_list}")

    if [ -e "${basedir}/po/CMakeLists.txt" ]; then
        echo -e "include(ECMOptionalAddSubdirectory)\necm_optional_add_subdirectory(po)" >>${basedir}/CMakeLists.txt
    fi
}
